# README
See LICENSE file for code and data licensing information.
This repository contains code written for julia v1.8 to simulate the growing rods model described in "Stress anisotropy in confined populations of growing rods" by Jonas Isensee, Lukas Hupe, Ramin Golestanian, and Philip Bittihn. ([link](https://arxiv.org/abs/2207.02607))
You can either download a zipped package from the gitlab page
of the repository or clone the code from the command line via
```
git clone https://gitlab.gwdg.de/LMP-pub/StressAnisotropyGrowingRods.git

```
into a directory of your choice.
To get started, ensure that julia is installed on your system. Navigate to the `examples` folder in this repository and start a julia REPL.
```
import Pkg
Pkg.activate(".")
Pkg.instantiate()
```

Two example simulation files are provided:
 - `simulation_live_plotting.jl` simulates a growing colony in a small domain and provides live plotting using `PyPlot.jl`.
 - `simulation_with_output.jl` is equivalent to the actual code used to produce simulation files for the publication. It takes a long list of parameters and writes system state snapshots into an output file. It additionally shows how one can use `InPartS` for extracting interaction forces from system states.

Either can be executed via
```
include(SCRIPTNAME)
```
