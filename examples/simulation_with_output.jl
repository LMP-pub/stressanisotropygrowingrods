using Pkg
Pkg.activate(@__DIR__)
cd(@__DIR__)
using StressAnisotropyPaperCode
using InPartS, JLD2
##

modelparams = (;
    #  I. Model parameters
    maxlength = 4,
    youngsmodulus = 1.6e6,
    inthardnessfactor = 1.0,
    intmobilityfactor = 1.0,
    growthratedistribution = StressAnisotropyPaperCode.Uniform(1.0, 0.5),
    viscosity = 0.05,
    )

fullparams = (;
    # II. Initial conditions
    ncols = 4,
    nrows = 4,
    nlayers = 1,
    BC = (Boundary,Boundary),
    width = 100,
    height = 100,
    obstacles = (InPartS.LeftWall(0.0), InPartS.RightWall(100.0)),

    #  IV. Simulation parameters
    celltype = RodCell,
    reg = SimpleSeqRegistrar(),
    trigger = PeriodicCallback(sim -> true, 0.1),
    rngdumpinterval = 50,
    dxmax = 0.02,
    dtmax = 0.01,
    io_backend = JLD2.JLDFile,
    daily_backups = true,
    threaded = (Threads.nthreads() > 1),
    autodelete = true,
    autogrid_kwargs = NamedTuple(),
    tmax = 6.0,
)


rundata = StressAnisotropyPaperCode.rodsimulation("test_simulation"; modelparams, params=fullparams, seed=42)

############################################################################################
## plotting snapshots from the simulation
############################################################################################

using PyPlot
pygui(true)

f = JLD2.jldopen("test_simulation.jld2")

# Load the simulation with the initial state (snapshot 0)
sim = InPartS.readsim(f; snap=0)
PyPlot.figure(figsize = (10, 10))
plot(sim)
PyPlot.savefig("snapshot_0.png", dpi = 100)

# load a different snapshot into the simulation object
InPartS.readsnap!(sim, f, 60)

# extract force dipoles
forces = []
InPartS.initialize_pc!(sim)
InPartS.computeforces!(forces, sim, )

# Clear plotting window
PyPlot.cla()
plot(sim)
quiver(map(x->x.rx + x.δx/2, forces), map(x->x.ry + x.δy/2, forces), map(x-> x.fx, forces), map(x-> x.fy, forces), scale = 2.5e4, color = "red", zorder = 150, width = 0.002)
quiver(map(x->x.rx - x.δx/2, forces), map(x->x.ry - x.δy/2, forces), map(x->-x.fx, forces), map(x->-x.fy, forces), scale = 2.5e4, color = "red", zorder = 150, width = 0.002)
savefig("snapshot_40.png", dpi = 300)

# close the file
close(f)
