using Pkg
Pkg.activate(@__DIR__)
# load InPartS and the bacteria model definitions
using InPartS, StressAnisotropyPaperCode

# plotting, static arrays, random numbers
using PyPlot, StaticArrays, Random

# Create a simulation container for rod cells,
# with an x-periodic 20×40 domain
sim = Simulation(ParticleObstacleContainer{2, RodCell, InPartS.AxisAlignedWall}();
    domainsize = (20, 30),
    boundaries = (Boundary, Boundary),
    rng = Random.Xoshiro(42) # seeded RNG for reproduceability
)

# add walls along the left and right domain boundaries
addobstacles!(sim, [
    InPartS.LeftWall(0.0),
    InPartS.RightWall(20.0)
])
# add a rod cell with random orientation and a division length of 4
# (max. backbone length bᵐᵃˣ = 3) to the system centre
addparticle!(sim;
    centerpos = SA[10.0, 15.0],
    φ = π*rand(sim.rng),
    params = RodCellParams(maxlength = 3, youngsmodulus = 8e5)
)

# setup live visualisation using the matplotlib plotting library
pygui(true)
cb = PeriodicCallback(InPartS.callback_plot(), 0.05)

# evolve system for 20 generations using adaptive time steps with dt ≤ 0.01
evolve!(sim, 9.0;
    tss = SimpleAdaptiveStepper(0.02, dt = 0.01),
    callback = cb
)
