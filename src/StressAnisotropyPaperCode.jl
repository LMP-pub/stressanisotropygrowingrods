module StressAnisotropyPaperCode

using InPartS
using InPartS.InPartSDev

using LinearAlgebra
using Printf
using Random
using StaticArrays
using SpecialFunctions
using Dates
using JLD2

## common definitions
abstract type AbstractRod{D} <: InPartS.AbstractParticle{D} end


include("common.jl")
include("distributions.jl")
include("rodcells/particles.jl")
include("obstacleinteractions.jl")
include("initialconditions.jl")

############################################################################################
## Simulation function
############################################################################################


logmsg(sim) = string(Dates.format(now(UTC), dateformat"yyyy-mm-dd HH:MM:SS"), " UTC | t = ", round(InPartS.current_time(sim), digits = 2), "; n = ", length(particles(sim)), "; maxrss = ", round(Sys.maxrss()/1048576, digits=2), " MiB;")

function get_ptc(celltype; obstacles)
    if !isnothing(obstacles)
        obstacle_type = eltype(obstacles)
        particle_container = InPartS.ParticleObstacleContainer{2, celltype, obstacle_type}()
        append!(particle_container.sov, obstacles)
    else
        particle_container = InPartS.SimpleParticleContainer{2, celltype}()
    end
    particle_container
end


function rodsimulation(simname; params, modelparams, seed)

    (; io_backend) = params
    suffix = io_backend <: JLD2.JLDFile ? ".jld2" :
             io_backend <: H5Wrapper    ? ".h5" :
             ArgumentError("Unknown File Format")

    filename = simname*suffix


    @info "$(simname): Setting up simulation\n"*
            prettysprintparams(params, indent = 4)*"\n"*
            prettysprintparams(modelparams, indent = 4)

    # setup RNG
    rng = Random.MersenneTwister(seed)

    celltype = params.celltype

    # create params
    p = InPartS.ParamType(celltype)(; modelparams...)

    domainsize = (params.width, params.height)


    sim = Simulation( get_ptc(celltype; params.obstacles);
        domainsize,
        boundaries = params.BC,
        registrar = params.reg,
        rng,
        params.threaded
    )
    @info "Created $(string(sim))"


    # create cells
    ncells = autogrid!(sim, [params.ncols, params.nrows]; params = p, rng, params.autogrid_kwargs...)

    @info "Adding $ncells particles to the simulation"
    InPartS.flushadditions!(sim)


    iowarn = parse(Bool, get(ENV, "CG_IO_WARN", "true"))
    cbs = InPartS.SaveCallback{io_backend}(params.trigger, filename, (s, sim) -> (s.nextsnap % params.rngdumpinterval == 0), iowarn)
    cbb = InPartS.BackupCallback(io_backend, filename*"_backup"; interval = 60*30.0, warn = iowarn)

    if params.daily_backups
        backup = (sim) -> begin
            backupname = joinpath(dirname(filename), "#"*basename(filename))
            cp(filename, backupname; force=true)
        end
        cbb = cbb ∘ InPartS.RealTimeCallback(backup, 86400.0)
    end

    cbb2 = InPartS.RealTimeCallback(300.0) do sim
        println(logmsg(sim)); flush(stdout); flush(stderr)
    end
    allcallbacks = cbb2 ∘ cbs ∘ cbb


    @info "$(simname): Starting simulation"
    flush(stderr); flush(stdout)

    runinfo = evolve!(sim, params.tmax - InPartS.current_time(sim);
        tss = SimpleAdaptiveStepper(dx = params.dxmax,
                                    dt = params.dtmax),
        callback = allcallbacks)

    @info "$(simname): Simulation finished; output saved to "*filename

    return (;
        simname,
        filename,
        params,
        modelparams,
        runinfo...)
end



end # module
