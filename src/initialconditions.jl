export rodgrid!, rectanglegrid!

## Enable autogrid! for AbstractRods

function InPartS.defaultensembleinitialization!(kwdict::Dict{Symbol, Any}, sim::Simulation{PTC, PT}, single_particle_params, rng) where {PTC, PT <: AbstractRod}
    # orientation
    get!(kwdict, _orientationname(sim.domain), [InPartS.randorientation(sim.domain, rng) for i ∈ 1:length(single_particle_params)])
    # growthrate
    get!(kwdict, :growthrate, map(x->rand(rng, x.growthratedistribution), single_particle_params))
    # growthprog
    gp = get!(kwdict, :growthprog, rand(rng, length(single_particle_params)))
    # nodeseparation
    get!(kwdict, :nodeseparation, map(x->restlength(x...), zip(InPartS._assert_length(gp, length(single_particle_params)), single_particle_params)))

    return kwdict
end


_orientationname(::AbstractDomain{2}) = :φ
_orientationname(::AbstractDomain{3}) = :orientation

InPartS.minimumgridconstant(::Type{<: AbstractRod}, p)  = p.maxlength + 2p.radius
