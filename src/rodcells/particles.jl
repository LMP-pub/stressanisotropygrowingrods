export RodCell, RodCellParams


## Param definition

"""
    RodCellParams
Parameters for [`RodCell`](@ref)s (and [`RodCell3D`]s(@ref)).


Refer to LH's Master's thesis (Gitlab project wiki) for a detailed description
of the model.

# Extended help

## List of fields
 * `maxlength`: nodeseparation of a mature cell (growthprog → 1)
 * `radius`: radius.
 * `youngsmodulus`: cell hardness
 * `inthardnessfactor`: internal hardness relative to external hardness. Default value is `1.0`.
 * `viscosity`: uniformly scales cell mobilities
 * `intmobilityfactor`: sets ratio between parallel translational and internal mobilities.
 * `growthrateretention`: weight of inherited component of child growth rates
 * `growthratedistribution`: distribution of random component of child growth rate

"""
struct RodCellParams <: AbstractParams

    # cell geometry
    maxlength::Float64
    radius::Float64

    # potential parameters
    youngsmodulus::Float64
    inthardnessfactor::Float64

    # friction parameters
    viscosity::Float64
    intmobilityfactor::Float64

    # growth rate
    growthrateretention::Float64
    growthratedistribution::Distribution{Float64}


    # precomputed intermediate quantities
    _hertz_int::Float64
    _minlength::Float64
    _deltalength::Float64

    RodCellParams(l, r, ym, ihf, η, imf, γr, γd) =
        new(l, r, ym, ihf, η, imf, γr, γd, ihf * _hertz_int_default(ym, r), l/2 - r, l/2 + r)
end

# default values
function RodCellParams(;
    maxlength = 3.0,
    radius = 0.5,
    youngsmodulus = 1e4,
    inthardnessfactor = NaN,
    viscosity = 0.05,
    intmobilityfactor = 1.0,
    growthrateretention = 0.0,
    growthratedistribution = Uniform(1.0, 0.5),
)
    RodCellParams(
        maxlength, radius, youngsmodulus, inthardnessfactor, viscosity, intmobilityfactor,
        growthrateretention, growthratedistribution
    )
end


function RodCellParams(p::RodCellParams; kwargs...)
    newargs = Pair{Symbol, Any}[]
    for k ∈ fieldnames(RodCellParams)
        if startswith(string(k), "_")
            continue
        end
        v = get(kwargs, k, getproperty(p, k))
        push!(newargs, k => v)
    end
    return RodCellParams(;newargs...)
end

@exportoff RodCellParams _hertz_int
@exportoff RodCellParams _minlength
@exportoff RodCellParams _deltalength
@exporttransform RodCellParams growthratedistribution Dict(value) StressAnisotropyPaperCode.Distribution(InPartS.readdict(data))
@genexport RodCellParams


## Particle definition
"""
    RodCell
A simple two-dimensional growing Spherocylinder (discorectangle?).

Refer to LH's Master's thesis (Gitlab project wiki) for a detailed description
of the model.

# Extended help

## List of fields

 * `id`, `centerpos`, `params`: InPartS standard fields

 * `nodeseparation`: length of the cell backbone measuerd between the centres of the circular caps
 * `φ`: angle of the cell backbone w.r.t. the x-Axis
 * `growthprog`: current growth phase of the cell
 * `grothrate`: rate of change of `growthprog`
 * `attached`: if set to true, evolution of every degree of freedom (except for `growthprog`) is stopped. _Currently unused_.
 * `backbone`: internal representation of the backbone as a `LineSegment2D`
 * `fP`, `fN`: node force accumulators
"""
Base.@kwdef mutable struct RodCell <: AbstractRod{2}
    id::UInt64
    centerpos::SV{Float64}
    nodeseparation::Float64 = NaN;
    φ::Float64
    growthprog::Float64 = 0.0
    growthrate::Float64 = 1.0
    attached::Bool = false

    backbone::LineSegment2D{Float64} = LineSegment2D(centerpos; orientation = φ, length = nodeseparation)
    # node forces
    fP::SV{Float64} = SA[0.0, 0.0]
    fN::SV{Float64} = SA[0.0, 0.0]

    params::RodCellParams

    function RodCell(id, cp, ns, φ, gp, gr, at, bb, fp, fn, p)
        isnan(ns) && (ns = restlength(gp, p); bb = LineSegment2D(cp; orientation = φ, length = ns))
        new(id, cp, ns, φ, gp, gr, at, bb, fp, fn, p)
    end
end

@exportoff RodCell fP
@exportoff RodCell fN
@exportoff RodCell backbone
@genexport RodCell

InPartS.PlotStyle(::Type{RodCell}) = IsPolyPlottable()

## InPartS API Functions

InPartS.ParamType(::Type{<:RodCell}) = RodCellParams

"""
    step!(cell::RodCell, dt, sim::Simulation)
Advances the state of the cell by `dt` time units.

Uses the forces currently saved in `cell.fP` and `cell.fN` and the mobilities as
computed by [`getmobilities`](@ref).
The backbone struct is replaced to match the new cell state.
Cells outside the boundaries are automatically registered for deletion, cells with a
`growthprog` ≥ 1 are replaced by their children.

If the cell is `attached`, all degrees of freedom except for the `growthprog` are
**not** evolved.  The cell can still divide if attached.
"""
function InPartS.step!(cell::RodCell, dt, sim::Simulation)
    #(isnan(w.μSattach) && isnan(w.μSdetach)) || update_attachment!(cell, w)

    # move cell if not attached to substrate
    if ! cell.attached

        Fpar, Fperp, Fax, τ = decompose_forces(cell)
        μpar, μperp, μrot = getmobilities(cell)

        # evolve parameters (project report, Eq. (20))
        cell.centerpos += dt*(μpar * Fpar + μperp * Fperp)
        if isfinite(cell.params.inthardnessfactor)
            cell.nodeseparation = min(cell.nodeseparation + dt*cell.params.intmobilityfactor * 2μpar * Fax, restlength(cell))
        else
            cell.nodeseparation = restlength(cell)
        end
        cell.φ += dt*μrot * τ

        if cell.nodeseparation < 0.0
            cell.nodeseparation = 0.0
        end

        cell.backbone = LineSegment2D(cell.centerpos; orientation = cell.φ, length = cell.nodeseparation)
    end

    cell.growthprog += dt*cell.growthrate

    # die if necessary
    if isoutside(cell.centerpos, sim.domain)
        rmparticle!(sim, cell.id)
    elseif cell.growthprog ≥ 1
        spawnchildren(cell, sim)
        rmparticle!(sim, cell.id)
    end
    return
end


"""
    InPartS.externalforces!(c1::RodCell, c2::RodCell, sim::Simulation)
Computes the interaction forces between two cells using [`closestpoints()`](@ref).

Forces are distributed over `fP` and `fN` using [`distributeforce!()`](@ref).
"""
@forcedef function InPartS.externalforces!(c1::RodCell, c2::RodCell, sim::Simulation)
    # find closest points modulo boundaries
    s, t = closestpoints(c1.backbone, c2.backbone, sim.domain,
                         asquare = nodeseparation(c1) * nodeseparation(c1),
                         bsquare = nodeseparation(c2) * nodeseparation(c2))

    # difference vector between closest points (again using MIC)
    deltavector = (c1.backbone(s) - c2.backbone(t)) % sim.domain
    distance = norm(deltavector)

    if distance == 0
        # this case is not well-defined
        return
    end
    # edge-to-edge distance
    dedge = distance - radius(c1) - radius(c2)

    # scalar force along unit vector
    if dedge ≤ 0
        # hertzian repulsion
        force = hertzfactor(c1, c2, sim)*dedge*sqrt(-dedge)*deltavector/distance

    else
        # nothing more to do
        return
    end

    # distribute forces over pseudo-nodes
    distributeforce!(c1, -force, s)
    distributeforce!(c2,  force, t)

    @force begin
        midpoint = fold_back(c2.backbone(t) + deltavector ./2, sim.domain)
        forcetuple(midpoint, force, -deltavector)
    end
    return
end

"""
    internalspring!(cell::RodCell, s::Simulation)
Computes the forces due to the internal spring using Hertzian
repulsion and adds them to the cell's `fN` and `fP` fields with appropriate signs.

The rest length of the spring is computed with  [`restlength`](@ref).
"""
@forcedef function InPartS.internalforces!(cell::RodCell, sim::Simulation)
    if !isfinite(cell.params.inthardnessfactor)
        # no forces today
        return
    end

    Δ = restlength(cell) - nodeseparation(cell)
    # force along unit vector
    force = unitvector(cell) * cell.params._hertz_int*Δ*sqrt(abs(Δ))

    cell.fP += force
    cell.fN -= force
    @force forcetuple(cell.centerpos, force, unitvector(cell)*nodeseparation(cell))
    return
end

"""
    distributeforce!(c::RodCell, force::SV{Float64}, r::Float64)
Distributes the given force over `fP` and `fN`, with `r*force` added to `fP` and
`(1-r)*force` added to `fN`
"""
@inline function distributeforce!(c::RodCell, force::SVector{2, Float64}, r)
    c.fP += force * r
    c.fN += force * (1 - r)
    return
end

"""
    interactionrange(ptype::Type{RodCell, sim::Simulation) → ir::Float64
Compute the range of the longest possible spatial interactions for [`RodCell`](@ref)s.
Uses the largest `maxlength` in the registered parameter structs.
"""
InPartS.interactionrange(::Type{RodCell}, sim::Simulation) = maximum(p -> p.maxlength + 2p.radius, sim.params)




## AbstractRod functions

unitvector(c::RodCell) = c.backbone.eφ
nodeseparation(c::RodCell) = c.nodeseparation

restlength(c::RodCell) = restlength(c.growthprog, c.params)
restlength(g, p::RodCellParams) = p._minlength + (p._deltalength)*g

## other stuff

"""
    spawnchildren(parent::RodCell, sim::Simulation)
Adds two new cells to the simulation, evenly spaced along the backbone of the parent
cell.

The new node separation is chosen to make the cells fit in length taken up by their parent
without.
The growth rate is randomly modified using the `growthrateretention` and a random
value drawn from the `growthratedistribution` using the simulation random number generator.

At the moment, all other parameters (except for the `id`) are inherited from the parent.
"""
function spawnchildren(parent::RodCell, sim::Simulation)
    # offset (in backbone parameter units) of the child cells
    coffset = parent.params.radius/(2*parent.nodeseparation)

    # Note that forces are not continuous at division, but this doesn't seem to be that important
    childlength = parent.nodeseparation/2 - parent.params.radius

    for (i, pos) in enumerate((0.25 - coffset, 0.75 + coffset))
        newcenter = parent.backbone(pos)

        # do not spawn outside domain boundaries
        if isoutside(newcenter, sim.domain)
            continue
        end

        # calculate new growth rate
        γnew = parent.params.growthrateretention*parent.growthrate +
            (1 - parent.params.growthrateretention)*rand(sim.rng, parent.params.growthratedistribution)

        # add child cell with new cell ID
        addparticle!(sim, RodCell,
            centerpos = newcenter,
            nodeseparation = childlength,
            φ = parent.φ,
            growthprog = 0,
            growthrate = γnew,
            attached = parent.attached,
            _parent_id = parent.id,
            params = parent.params
        )

        #fold_back!(child, w)
        #push!(children, child)
    end
end




## Pretty printing

#one-line text output
Base.show(io::IO, cell::RodCell) = print(io, "RodCell(r ≈ [$(join(nicestrlong.(cell.centerpos), ","))], l ≈ $(nicestrshort(cell.nodeseparation)), φ ≈ $(nicestrshort(cell.φ)))")
Base.show(io::IO, p::RodCellParams) = print(io, "RodCellParams(l ≈ $(nicestrshort(p.maxlength)), Y ≈ $(nicestrsci(p.youngsmodulus)), r ≈ $(nicestrshort(p.radius)))")

# fancy multi-line output
Base.show(io::IO, ::MIME"text/plain", cell::RodCell) = print(io,
     """RodCell $(repr(cell.id))
     r ≈ [$(join(nicestrlong.(cell.centerpos), ","))]
     l ≈ $(nicestrshort(cell.nodeseparation));  φ ≈ $(nicestrshort(cell.φ))
     g ≈ $(nicestrshort(cell.growthprog));  γ = $(nicestrshort(cell.growthrate))
     $(cell.attached ? "" : "not ")attached
     params: $(cell.params)"""
)


"""
    getpolygon(cell::RodCell,res = 50)
Returns a list of `res` points that describe a polygon approximating the
shape of a 2-dimensional `RodCell`. `res` should be an even number.
"""
InPartS.getpolygon(cell::RodCell; res = 50) = dumbbellpolygon(radius(cell), cell.φ, first(cell.backbone), last(cell.backbone), α = π/2; res)
