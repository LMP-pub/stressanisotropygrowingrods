
## common InPartS API functions

@inline radius(cell::AbstractRod) = cell.params.radius

"""
    reset!(cell::AbstractRod, sim::Simulation)
Clears the node forces to prepare the cell for the next time step
"""
function InPartS.reset!(cell::AbstractRod{D}, ::Simulation) where D
    cell.fP = zero(SVector{D, Float64})
    cell.fN = zero(SVector{D, Float64})
    return
end


"""
    InPartS.maxvelocity(::Type{<:AbstractRod}, sim::Simulation)
Heuristically estimate maximum velocity in a simulation of `AbstractRod`s.

# Extended help

This heuristic uses a simple estimate for the maximum translational mobility
(see [`getmobilities`](@ref)), with a magic number that was determined using
the highly scientific methods of Looking At A Graph and Eyeballing It.

It then finds the maximum node force in the system and multiplies it with the
mobility estimate to obtain a decent estimate for the maximum node velocity.

!!! note

    The maximum mobility estimate assumes a minimum `nodeseparation` of 0.0.
    This is not true for `Spherocylinder`s, so maximum velocity is slightly
    overestimated for them.

"""
function InPartS.maxvelocity(::Type{<:AbstractRod}, sim::Simulation)
    minradius = Inf
    minviscosity = Inf
    maximf = -Inf
    for p ∈ sim.params
        (p.radius < minradius) && (minradius = p.radius)
        (p.intmobilityfactor > maximf) && (maximf = p.intmobilityfactor)
        (p.viscosity < minviscosity) && (minviscosity = p.viscosity)
    end

    maxmobility = max(1, maximf) * 0.102/(2minradius)/minviscosity

    maxforce = 0.0
    for cell ∈ particles(sim)
        # manually unrolled loop over pseudo-nodes
        nf = norm(cell.fP)
        if nf > maxforce
            maxforce = nf
        end

        nf = norm(cell.fN)
        if nf > maxforce
            maxforce = nf
        end
    end
    return maxforce * maxmobility
end



## Hertz factor computation

"""
    invsum(b...)
Compute the inverse sum of the inverse of the arguments.
"""
invsum(a::Vararg{Number}) = inv(sum(inv, a))

"""
    ⊞(a, b)
Compute the inverse sum of the inverse `1/(1/a + 1/b)` (see [`invsum`](@ref)).
"""
⊞(a::Number,b::Number) = invsum(a, b)


@doc raw"""
    hertzfactor(c1::AbstractRod, c2::AbstractRod, sim)
Computes the Hertzian interaction factor of c1 and c2 from their radii and Young's moduli.

# Extended help

The Hertz factor is defined as
```math
H = \frac{1}{1/Y_1 + 1/Y_2} \cdot \sqrt{\frac{1}{1/R_1 + 1/R_2}}
```
"""
function hertzfactor(c1::AbstractRod, c2::AbstractRod, sim::Simulation)
    if InPartS.equalparams(c1.params, c2.params, sim)
        return c1.params.youngsmodulus/2 * sqrt(radius(c1)/2)
    else
        return (c1.params.youngsmodulus ⊞ c2.params.youngsmodulus) * sqrt(radius(c1) ⊞ radius(c2))
    end
end

"""
    hertzfactor(c::AbstractRod, sim)
Compute the Hertzian interaction factor for interactions of `c` with an infinitely hard, flat object.
Used for [`obstacleforces!`](@ref).
"""
function hertzfactor(c::AbstractRod, ::Simulation)
    return c.params.youngsmodulus * sqrt(radius(c))
end

# for internal hardness computation
@inline _hertz_int_default(ym, r) = ym/2 * √(r/2)




## Common stuff


"""
    decompose_forces(cell::AbstractRod) → Fpar::SV{Float64}, Fperp::SV{Float64}, Fax::Float64, τ::Float64
Compute the decomposed forces acting on the individual degrees of freedom.
Returns
 * `Fpar`: the COM force vector parallel to the cell orientation
 * `Fperp`: the COM force vector perpendicular to the cell orientation
 * `Fax`: the __scalar__ compression force along the cell axis
 * `τ`: the __scalar__ torque on the cell

For a detailed description of the force decomposition, see
    L. Hupe, Master's thesis, _Ordering principles in growing populations of rod-shaped particles_, 2021.
"""
function decompose_forces(cell::AbstractRod)
    rhat = unitvector(cell)

    # symmetric forces
    Fcom = cell.fP + cell.fN
    Fpar = dot(Fcom, rhat) * rhat
    Fperp = Fcom - Fpar

    # antisymmetric forces
    Fa = cell.fP - cell.fN
    Fax = dot(Fa, rhat)
    τ = 0.5 * nodeseparation(cell) * (rhat × Fa)

    return Fpar, Fperp, Fax, τ
end



## mobilities


"""
    rodmobilities(cell::AbstractRod) → μpar::Float64, μperp::Float64, μrot::Float64
Compute the cell mobilities using the Tirado-García de la Torre formulas[^1]

[^1]: Tirado, Martínez and García de La Torre, J. Chem. Phys. 81, 2047 (1984);
    [https://doi.org/10.1063/1.447827](https://doi.org/10.1063/1.447827)

Returns
 * `μpar`: the mobility parallel to the cell axis
 * `μperp`: the mobility perpendicular to the cell axis
 * `μrot`: the mobility of the rotational degree of freedom
"""
@inline function rodmobilities(len, a, η)
    loga = log(a); inva = 1/a; inva2 = inva^2
    return (
          (loga - 0.207 + 0.980*inva - 0.133*inva2)/(2π * η * len),
          (loga + 0.839 + 0.185*inva + 0.233*inva2)/(4π * η * len),
        3*(loga - 0.662 + 0.917*inva - 0.050*inva2)/( π * η * len^3)
    )
end


# use rod mobilities for abstract rods by default
@inline function getmobilities(cell::AbstractRod)
    len = nodeseparation(cell) + 2cell.params.radius
    a = len/2cell.params.radius
    return rodmobilities(len, a, cell.params.viscosity)
end

# computes the change in orientation vector eφ due to a torque τ in 2D
@inline _torque_to_deφ(τ::Real, eφ::AbstractVector) = @inbounds τ * [-eφ[2], eφ[1]]

## Polygon computation

function arcpolygon!(vectors::AbstractMatrix{T}, r::T, θmin::T;
    center::AbstractVector{T} = zeros(T, 2),
    offset::Int = 0,
    res::Int = 50,
    Δθ::T = 2π/(res-1),
) where {T <: AbstractFloat}
    # rotation matrix
    sΔ, cΔ = sincos(Δθ)
    𝐑 = SA[cΔ -sΔ; sΔ cΔ]

    # normal vector
    sm, cm = sincos(θmin)
    𝐧 = SA[r*cm, r*sm]

    vectors[offset+1, :] = center + 𝐧
    @inbounds for i ∈ 2:res
        𝐧 = 𝐑*𝐧
        vectors[offset+i, :] = center + 𝐧
    end
    return
end

arcpolygon(args...;res = 50, kwargs...) = arcpolygon!(Array{Float64}(undef,res, 2), args...; kwargs...)
arcpolygon_f0(args...;res = 50, kwargs...) = arcpolygon!(Array{Float32}(undef,res, 2), args...; kwargs...)


function dumbbellpolygon!(points::AbstractMatrix{T}, r, φ, p1, p2; α::Float64 = π/2, res::Int = size(points)[1])  where {T <:AbstractFloat}
    @assert res % 2 == 0 "`res` must be a multiple of 2"
    arcpoints = res÷2
    Δθ = 2α/(arcpoints-1)
    # draw arcs
    arcpolygon!(points, r, φ - α + π; center = p1, Δθ = Δθ, res = arcpoints, offset = 0)
    arcpolygon!(points, r, φ - α;     center = p2, Δθ = Δθ, res = arcpoints, offset = arcpoints)
    return points
end

dumbbellpolygon(args...;res = 50, kwargs...) = dumbbellpolygon!(Array{Float64}(undef,res, 2), args...; kwargs...)
dumbbellpolygon_f0(args...;res = 50, kwargs...) = dumbbellpolygon!(Array{Float32}(undef,res, 2), args...; kwargs...)


## Pretty Printing

nicestrlong(num::AbstractFloat) = @sprintf "% 7.2f" num
nicestrshort(num::AbstractFloat) = @sprintf "% 5.2f" num

nicestrsci(num::AbstractFloat) = @sprintf "% .4e" num

function prettysprintparams(params::NamedTuple; indent = 0)
    indentstr = " "^indent
    substrings = String[]
    padlength = maximum(length.(string.(keys(params))))
    for (k, v) ∈ pairs(params)
        str = string(k)
        pad = " "^(padlength - length(str))
        push!(substrings, indentstr*str*pad*" = "*string(v))
    end

    return join(substrings, '\n')
end
