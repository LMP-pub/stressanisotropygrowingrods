using Random
import Base.rand


"""
abstract type for probability distributions.
"""
abstract type Distribution{T} end


"""
    rand(rng::AbstractRNG, dist::Distribution)
Sample a random number from the [`Distribution`](@ref)
"""
rand(rng::AbstractRNG, dist::Distribution)



################################################################################
##  LIBRARY
################################################################################


## UNIFORM DISTRIBUTION ########################################################

"""
    Uniform(c, w)
    Uniform(; center::T, width::T) where {T <: AbstractFloat}
Uniform [`Distribution`](@ref) with width `w` centered on `c`
"""
struct Uniform{T <: AbstractFloat} <: Distribution{T}
    center::T
    width::T

    # constructor
    Uniform(c::T, w::T) where {T <: AbstractFloat} = new{T}(c, w)
    Uniform{T}(c::Real, w::Real) where {T <: AbstractFloat} = new{T}(T(c), T(w))
end

Uniform(; center::T, width::T) where {T <: AbstractFloat} = Uniform(center, width)
Uniform{T}(; center::T, width::T) where {T <: AbstractFloat} = Uniform(center, width)

rand(rng::AbstractRNG, dist::Uniform{T}) where {T <: AbstractFloat} = (dist.width*(rand(rng, T) - 0.5) + dist.center)::T

"""
    Dict(dist<:Distribution) → dict::Dict{String, Any}
Converts a distribution object to a dictionary associating the field names with
their values.
The type of the distribution is saved using the key `"distributionType"`.

Dictionaries created using this function can be converted back to a distribution
using [`Distribution(::Dict{String, Any}`](@ref).
"""
function Base.Dict(dist::D) where {D <: Distribution}
    d = Dict{String, Any}(string(fn) => getproperty(dist, fn) for fn ∈ fieldnames(D))
    d["distributionType"] = string(D)
    return d
end


"""
    Distribution(dict::Dict{String, Any}) → dist::Distribution
Creates a new distribution from a dictionary. This function can be used for all
subtypes of `Distribution` that provide a constructor accepting all fields as
keyword arguments.

The type of the distribution is parsed from the value of `dict["distributionType"]`.
 Note that type parameters specified in this string are discarded. They should
therefore be inferrable from the other field values.

The type parsing uses [`reconstruct_subtype()`](@ref) to find all the concrete
leaves of the tree below the type `Distribution`.
"""
function Distribution(dict::Dict{String, <:Any})
    # make copy of dictionary
    dcopy = Dict(Symbol(k) => v for (k,v) in dict)
    # get type
    type = reconstruct_subtype(pop!(dcopy, :distributionType), Distribution)

    return type(;dcopy...)
end
