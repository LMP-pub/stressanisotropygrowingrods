###########################################################################################
#                              Axis Aligned Infinite Walls                                #
###########################################################################################

# these function are undocumented because they are boring
# they all do the same thing for slightly different obstacles and particles

# rods
@forcedef function InPartS.obstacleforces!(so::AxisAlignedWall{N,C,D}, c::RodCell, sim::Simulation) where {N,C,D}
    # which node is closest to the wall?
    s =  D*c.backbone.eφ[C]
    node = s < 0 ? 0.0 : 1.0

    # project relevant node position onto relevant axis
    compval = c.centerpos[C] + D*abs(s)*nodeseparation(c)/2

    signeddistance = D * (so.val - compval)
    dedge = signeddistance - radius(c)

    if dedge ≤ 0
        # hertzian repulsion
        force = hertzfactor(c, sim)*dedge*sqrt(-dedge) * InPartS.normalvector(so)
    else
        return
    end

    # add force to rod
    distributeforce!(c, -force, node)

    # force extraction
    @force begin
        midpoint = fold_back(c.backbone(node) - signeddistance/2 * InPartS.normalvector(so), sim.domain)
        forcetuple(midpoint, -force, signeddistance * InPartS.normalvector(so) )
    end

    return
end
